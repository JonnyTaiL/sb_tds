// Copyright Epic Games, Inc. All Rights Reserved.

#include "SB_TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "SB_TDS/Game/MyGameInstance.h"

ASB_TDSCharacter::ASB_TDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ASB_TDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/


	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			 
			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}




	MovementTick(DeltaSeconds);
}


void ASB_TDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}


void ASB_TDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ASB_TDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ASB_TDSCharacter::InputAxisY);

	/*NewInputComponent->BindAction(TEXT("Aim"), IE_Pressed, this, &ASB_TDSCharacter::InputAimPressed);
	NewInputComponent->BindAction(TEXT("Aim"), IE_Released, this, &ASB_TDSCharacter::InputAimReleased);*/
	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Pressed, this, &ASB_TDSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Released, this, &ASB_TDSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), IE_Pressed, this, &ASB_TDSCharacter::TryReloadWeapon);
}

void ASB_TDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ASB_TDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ASB_TDSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ASB_TDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

//void ASB_TDSCharacter::InputAimPressed()
//{
//	
//}
//
//
//void ASB_TDSCharacter::InputAimReleased()
//{
//
//}
//



void ASB_TDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);


	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult ResultHit;
		//MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

		if(SprintEnabled == false)
		{
			float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
		}

		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			switch (MovementState)
			{
			case EMovementState::Aim_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::AimWalk_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::Walk_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::Run_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);

				break;
			case EMovementState::Sprint_State:
				break;
			default:
				break;

			}

			CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
		}
	}

	//if (CurrentWeapon)
	//{
	//	if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
	//	{
	//		CurrentWeapon->ShouldReduceDispersion = true;
	//	}
	//	else
	//	{
	//		CurrentWeapon->ShouldReduceDispersion = false;
	//	}
	//}



}




void ASB_TDSCharacter::CharacterUpdate()
{

	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}




	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ASB_TDSCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
		GetCharacterMovement()->bOrientRotationToMovement = false;
	}
	else
	{
		if (SprintEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::Sprint_State;
			GetCharacterMovement()->bOrientRotationToMovement = true;
		}
		if (WalkEnabled && !SprintEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
			GetCharacterMovement()->bOrientRotationToMovement = false;
		}
		else
		{
			if (WalkEnabled && !SprintEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
				GetCharacterMovement()->bOrientRotationToMovement = false;
			}
			else
			{
				if (!WalkEnabled && !SprintEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
					GetCharacterMovement()->bOrientRotationToMovement = false;
				}
			}
		}
	}

	CharacterUpdate();

	//Weapon State Update

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}


}



void ASB_TDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check Melee or Range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("ASB_TDSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}

}

AWeaponDefault* ASB_TDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ASB_TDSCharacter::InitWeapon(FName IdWeapon)
{

	UMyGameInstance* myGI = Cast<UMyGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if(myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{ 


			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();


				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation,
					&SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->UpdateStateWeapon(MovementState);


					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ASB_TDSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ASB_TDSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ASB_TDSCharacter::WeaponFireStart);
				}
			}
		}
		else
		{
			//UE_LOG(LogTemp, Warning, TEXT("ASB_TDSCharacter::InitWeapon - Weapon not found in table -NULL"))
		}
	}
}


void ASB_TDSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if(!CurrentWeapon->WeaponReloading)
		{
			if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound)
			{
				CurrentWeapon->InitReload();
			}
		}
		
	}
}


void ASB_TDSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ASB_TDSCharacter::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void ASB_TDSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	WeaponFireStart_BP(Anim);
}

void ASB_TDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ASB_TDSCharacter::WeaponReloadEnd_BP_Implementation()
{
	//in BP
}

void ASB_TDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}


UDecalComponent* ASB_TDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}





