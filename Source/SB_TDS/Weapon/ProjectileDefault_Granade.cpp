// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Granade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TDS.DebugExplode"),
	DebugExplodeShow, TEXT("Draw Debug For Explode"), ECVF_Cheat);

//
//AProjectileDefault_Granade::AProjectileDefault_Granade()
//{
//
//
//
//}



void AProjectileDefault_Granade::BeginPlay()
{
	Super::BeginPlay();

	ImpactProjectile();
}

void AProjectileDefault_Granade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplode(DeltaTime);
}

void AProjectileDefault_Granade::TimerExplode(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplode > TimeToExplode)
		{
			//Explode
			Explode();
		}
		else
		{
			TimerToExplode += DeltaTime;
		}
	}

}

void AProjectileDefault_Granade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpuls, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpuls, Hit);
}

void AProjectileDefault_Granade::ImpactProjectile()
{
	//Init Granade
	TimerEnabled = true;
}

void AProjectileDefault_Granade::Explode()
{

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}


	TimerEnabled = false;
	if (ProjectileSettings.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplodeFX,GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSettings.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExplodeSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSettings.ExplodeMaxDamage,
		ProjectileSettings.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		1000.0f,
		2000.0f,
		5,
		NULL, IgnoredActor, nullptr, nullptr);

	this->Destroy();
}
