// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SB_TDS/FuncLibrary/Types.h"
#include "ProjectileDefault.h"
#include "Components/ArrowComponent.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);


UCLASS()
class SB_TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();


	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireInfo")
		FAdditionalWeaponInfo WeaponInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);

	void WeaponInit();

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);

		bool CheckWeaponCanFire();

		FProjectileInfo GetProjectile();
		UFUNCTION()
			void Fire();


		void UpdateStateWeapon(EMovementState NewMovementState);
		void ChangeDispersionByShoot();
		float GetCurrentDispersion()const;
		FVector ApplyDispersionToShoot(FVector DirectionShoot)const;


		FVector GetFireEndLocation()const;
		int8 GetNumberProjectileByShot() const;

		//Timers
		float FireTimer = 0.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;
	



		
		//flags
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
			bool WeaponFiring = false;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
			bool WeaponAiming = false;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
			bool WeaponReloading = false;

		bool BlockFire = false;
		//Dispersion
		bool ShouldReduceDispersion = false;
		float CurrentDispersion = 0.0f;
		float CurrentDispersionMax = 1.0f;
		float CurrentDispersionMin = 0.1f;
		float CurrentDispersionRecoil = 0.1f;
		float CurrentDispersionReduction = 0.1f;

		//Timer drop clip on reload
		bool DropClipFlag = false;
		float DropClipTimer = -1.0f;

		//Shell Flag
		bool DropShellFlag = false;
		float DropShellTimer = -1.0f;

		FVector ShootEndLocation = FVector(0);


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
			float SizeVectorToChangeShootDirection = 100.0f;


		UFUNCTION(BlueprintCallable)
			int32 GetWeaponRound();

		UFUNCTION()
			void InitReload();
			void FinishReload();

			UFUNCTION()
				void InitDropMesh(UStaticMesh* DropMesh, float DropMeshLifeTime, FTransform Offset, FVector DropImpulseDirection, float DropImpulsePower, float DropImpulseDispersion, float DropMeshMass);
};
