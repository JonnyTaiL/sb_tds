// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "ProjectileDefault_Granade.generated.h"

/**
 * 
 */
UCLASS()
class SB_TDS_API AProjectileDefault_Granade : public AProjectileDefault
{
	GENERATED_BODY()
	

public:

	UPROPERTY(VisibleAnywhere)
		UProjectileMovementComponent* ProjectileMovementComponent;


protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	void TimerExplode(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpuls, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	void Explode();

	bool TimerEnabled = false;
	float TimerToExplode = 0.0f;
	float TimeToExplode = 5.0f;
};
