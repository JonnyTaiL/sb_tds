// Copyright Epic Games, Inc. All Rights Reserved.

#include "SB_TDSGameMode.h"
#include "SB_TDSPlayerController.h"
#include "SB_TDS/Character/SB_TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASB_TDSGameMode::ASB_TDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASB_TDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}