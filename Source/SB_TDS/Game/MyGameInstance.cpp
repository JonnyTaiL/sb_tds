// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstance.h"

bool UMyGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo)
{
	bool bIsFound = false;
	FWeaponInfo* WeaponInfoRow;

	WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "", false);

	if (WeaponInfoRow)
	{
		bIsFound = true;
		OutInfo = *WeaponInfoRow;
	}

	return bIsFound;
}
